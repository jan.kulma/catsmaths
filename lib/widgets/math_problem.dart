import 'dart:async';
import 'dart:math';

import 'package:catsmaths/constants.dart';
import 'package:catsmaths/state/maths_screen_state.dart';
import 'package:catsmaths/widgets/keyboard.dart';
import 'package:catsmaths/widgets/settings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const String lessOrEqualUnicode = '\u2264';
const String moreOrEqualUnicode = '\u2265';

class MathProblemModel {
  static const ADD = '+';
  static const SUBTRACT = '-';
  DifficultyLevel difficultyLevel;
  late String stringRepresentation;
  late int correctAnswer;

  MathProblemModel({required this.difficultyLevel}) {
    switch (difficultyLevel) {
      case DifficultyLevel.cat:
        generateCatLevel();
        break;
      case DifficultyLevel.tiger:
        generateTigerLevel();
        break;
      case DifficultyLevel.lion:
        generateLionLevel();
        break;
    }
  }

  void generateCatLevel() {
    Random random = Random(DateTime.now().millisecondsSinceEpoch);
    int addUpTo = 20;
    int subtractUpFrom = 10;

    if (random.nextInt(2) == 1) {
      // 50%
      String operation = SUBTRACT;
      int a = random.nextInt(subtractUpFrom + 1);
      int b = random.nextInt(subtractUpFrom + 1);
      if (a > b) {
        correctAnswer = a - b;
        stringRepresentation = '${a.toString()} $operation ${b.toString()}';
      } else {
        correctAnswer = b - a;
        stringRepresentation = '${b.toString()} $operation ${a.toString()}';
      }

      return;
    }

    int a = random.nextInt(addUpTo + 1);
    int b = random.nextInt(addUpTo - a + 1);
    String operation = ADD;

    if (random.nextInt(3) <= 1) {
      // 66%
      correctAnswer = a + b;
      stringRepresentation = '${a.toString()} $operation ${b.toString()}';
      return;
    }

    int c = random.nextInt(addUpTo - a - b + 1);
    correctAnswer = a + b + c;
    stringRepresentation =
        '${a.toString()} $operation ${b.toString()} $operation ${c.toString()}';
    return;
  }

  void generateTigerLevel() {
    Random random = Random(DateTime.now().millisecondsSinceEpoch);
    int addUpTo = 30;
    int subtractUpFrom = 15;

    if (random.nextInt(2) == 1) {
      // 50%
      String operation = SUBTRACT;
      int a = random.nextInt(subtractUpFrom + 1);
      int b = random.nextInt(subtractUpFrom + 1);
      if (a > b) {
        correctAnswer = a - b;
        stringRepresentation = '${a.toString()} $operation ${b.toString()}';
      } else {
        correctAnswer = b - a;
        stringRepresentation = '${b.toString()} $operation ${a.toString()}';
      }

      return;
    }

    int a = random.nextInt(addUpTo + 1);
    int b = random.nextInt(addUpTo - a + 1);
    String operation = ADD;

    if (random.nextInt(3) <= 1) {
      // 66%
      correctAnswer = a + b;
      stringRepresentation = '${a.toString()} $operation ${b.toString()}';
      return;
    }

    int c = random.nextInt(addUpTo - a - b + 1);
    correctAnswer = a + b + c;
    stringRepresentation =
        '${a.toString()} $operation ${b.toString()} $operation ${c.toString()}';
    return;
  }

  void generateLionLevel() {
    Random random = Random(DateTime.now().millisecondsSinceEpoch);
    int addUpTo = 40;
    int subtractUpFrom = 20;

    if (random.nextInt(2) == 1) {
      // 50%
      String operation = SUBTRACT;
      int a = random.nextInt(subtractUpFrom + 1);
      int b = random.nextInt(subtractUpFrom + 1);
      if (a > b) {
        correctAnswer = a - b;
        stringRepresentation = '${a.toString()} $operation ${b.toString()}';
      } else {
        correctAnswer = b - a;
        stringRepresentation = '${b.toString()} $operation ${a.toString()}';
      }

      return;
    }

    int a = random.nextInt(addUpTo + 1);
    int b = random.nextInt(addUpTo - a + 1);
    String operation = ADD;

    if (random.nextInt(3) <= 1) {
      // 66%
      correctAnswer = a + b;
      stringRepresentation = '${a.toString()} $operation ${b.toString()}';
      return;
    }

    int c = random.nextInt(addUpTo - a - b + 1);
    correctAnswer = a + b + c;
    stringRepresentation =
        '${a.toString()} $operation ${b.toString()} $operation ${c.toString()}';
    return;
  }

  bool validateAnswer(String userAnswer) {
    try {
      int parsedAnswer = int.parse(userAnswer);
      return parsedAnswer == correctAnswer;
    } catch (e) {
      return false;
    }
  }
}

class MathProblemController {
  static const String EVENT_CORRECT_ANSWER = 'solved';
  static const String EVENT_DIFFICULTY_CHANGED = 'difficulty_changed';
  MathProblemModel? model;
  String feedback = '';
  String answer = '';
  DifficultyLevel difficultyLevel = DifficultyLevel.cat;
  StreamController mathProblemStreamController =
      new StreamController.broadcast();

  Stream get mathProblemStream => mathProblemStreamController.stream;

  String get userAnswer {
    return answer == '' ? '?' : answer;
  }

  void addEvent(entered) {
    mathProblemStreamController.add(entered);
  }

  MathProblemModel getProblem(DifficultyLevel possiblyNewLevel) {
    if (difficultyLevel != possiblyNewLevel) {
      difficultyLevel = possiblyNewLevel;
      model = null;
    }
    if (model != null) {
      return model!;
    }
    return model = MathProblemModel(difficultyLevel: difficultyLevel);
  }

  String getFeedback() {
    return feedback;
  }

  void processInput(String input) {
    if (ACTION_KEYS.contains(input)) {
      parseAction(input);
      return;
    }
    parseAnswer(input);
  }

  void parseAnswer(String input) {
    if (answer.length > 2) {
      return;
    }
    answer += input;
  }

  void parseAction(String input) {
    switch (input) {
      case KEY_CLEAR:
        answer = '';
        feedback = '';
        break;
      case KEY_CHECK:
        if (model!.validateAnswer(answer)) {
          feedback = ':)';
          addEvent(EVENT_CORRECT_ANSWER);
        } else {
          feedback = ':(';
        }
        break;
    }
  }
}

class MathProblemView extends StatelessWidget {
  final MathProblemController controller;

  const MathProblemView({required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RichText(
          text: TextSpan(
            style: kMathsKeyboardStyle,
            text: controller
                .getProblem(context.watch<MathsScreenState>().difficultyLevel)
                .stringRepresentation,
            children: [
              TextSpan(text: ' = ${controller.userAnswer}'),
            ],
          ),
        ),
        SizedBox(
          height: 30.0,
        ),
        getRainbowText2(controller.feedback),
      ],
    );
  }
}
