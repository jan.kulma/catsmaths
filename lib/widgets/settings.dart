import 'package:catsmaths/constants.dart';
import 'package:catsmaths/state/maths_screen_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum DifficultyLevel { cat, tiger, lion }

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              DifficultySetting(
                text: ['+ 20', '10 -'],
                isSelected: false,
                onTap: () {
                  context
                      .read<MathsScreenState>()
                      .setDifficultyLevel(DifficultyLevel.cat);
                  Navigator.pop(context);
                },
                imgPath: 'assets/cat.png',
                imgHeight: 160,
                bgColor: kRainbowColors[6],
              ),
              SizedBox(height: 20.0),
              DifficultySetting(
                text: ['+ 30', '15 -'],
                isSelected: false,
                onTap: () {
                  context
                      .read<MathsScreenState>()
                      .setDifficultyLevel(DifficultyLevel.tiger);
                  Navigator.pop(context);
                },
                imgPath: 'assets/tiger.png',
                imgHeight: 160,
                bgColor: kRainbowColors[7],
              ),
              SizedBox(height: 20.0),
              DifficultySetting(
                text: ['+ 40', '20 -'],
                isSelected: false,
                onTap: () {
                  context
                      .read<MathsScreenState>()
                      .setDifficultyLevel(DifficultyLevel.lion);
                  Navigator.pop(context);
                },
                imgPath: 'assets/lion.png',
                imgHeight: 180,
                bgColor: kRainbowColors[8],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DifficultySetting extends StatelessWidget {
  final List<String> text;
  final bool isSelected;
  final void Function()? onTap;
  final String imgPath;
  final double imgHeight;
  final Color bgColor;

  const DifficultySetting({
    required this.text,
    required this.isSelected,
    required this.onTap,
    required this.imgPath,
    required this.imgHeight,
    required this.bgColor,
  });

  @override
  Widget build(BuildContext context) {
    List<Widget> texts = [];

    for (String line in text) {
      texts.add(Text(
        line,
        style: kLogoTextStyle.copyWith(fontSize: 55.0, color: Colors.white),
      ));
    }

    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(40.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[500]!.withOpacity(0.4),
              spreadRadius: 2,
              blurRadius: 4,
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 25.0,
            vertical: 20.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Image(
                image: AssetImage(imgPath),
                height: imgHeight,
              ),
              Column(
                children: texts,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
