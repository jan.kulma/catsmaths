import 'package:catsmaths/constants.dart';
import 'package:flutter/material.dart';

class Abacus extends StatefulWidget {
  @override
  _AbacusState createState() => _AbacusState();
}

class _AbacusState extends State<Abacus> {
  static double beadWidth = 40;
  static const POSITION_LEFT = 'left';
  static const POSITION_RIGHT = 'right';

  List beadsConfig2 = [];

  List beadsConfig = [
    {
      'left': 5 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '1',
      'color': kRainbowColors[0],
      'moveLeftMultiplier': 0,
      'moveRightMultiplier': 5,
    },
    {
      'left': 4 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '2',
      'color': kRainbowColors[0],
      'moveLeftMultiplier': 1,
      'moveRightMultiplier': 4,
    },
    {
      'left': 3 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '3',
      'color': kRainbowColors[1],
      'moveLeftMultiplier': 2,
      'moveRightMultiplier': 3,
    },
    {
      'left': 2 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '4',
      'color': kRainbowColors[1],
      'moveLeftMultiplier': 3,
      'moveRightMultiplier': 2,
    },
    {
      'left': 1 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '5',
      'color': kRainbowColors[2],
      'moveLeftMultiplier': 4,
      'moveRightMultiplier': 1,
    },
    {
      'left': 5 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '6',
      'color': kRainbowColors[2],
      'moveLeftMultiplier': 0,
      'moveRightMultiplier': 5,
    },
    {
      'left': 4 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '7',
      'color': kRainbowColors[3],
      'moveLeftMultiplier': 1,
      'moveRightMultiplier': 4,
    },
    {
      'left': 3 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '8',
      'color': kRainbowColors[3],
      'moveLeftMultiplier': 2,
      'moveRightMultiplier': 3,
    },
    {
      'left': 2 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '9',
      'color': kRainbowColors[4],
      'moveLeftMultiplier': 3,
      'moveRightMultiplier': 2,
    },
    {
      'left': 1 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '10',
      'color': kRainbowColors[4],
      'moveLeftMultiplier': 4,
      'moveRightMultiplier': 1,
    },
    {
      'left': 5 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '11',
      'color': kRainbowColors[5],
      'moveLeftMultiplier': 0,
      'moveRightMultiplier': 5,
    },
    {
      'left': 4 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '12',
      'color': kRainbowColors[5],
      'moveLeftMultiplier': 1,
      'moveRightMultiplier': 4,
    },
    {
      'left': 3 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '13',
      'color': kRainbowColors[6],
      'moveLeftMultiplier': 2,
      'moveRightMultiplier': 3,
    },
    {
      'left': 2 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '14',
      'color': kRainbowColors[6],
      'moveLeftMultiplier': 3,
      'moveRightMultiplier': 2,
    },
    {
      'left': 1 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '15',
      'color': kRainbowColors[7],
      'moveLeftMultiplier': 4,
      'moveRightMultiplier': 1,
    },
    {
      'left': 5 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '16',
      'color': kRainbowColors[7],
      'moveLeftMultiplier': 0,
      'moveRightMultiplier': 5,
    },
    {
      'left': 4 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '17',
      'color': kRainbowColors[8],
      'moveLeftMultiplier': 1,
      'moveRightMultiplier': 4,
    },
    {
      'left': 3 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '18',
      'color': kRainbowColors[8],
      'moveLeftMultiplier': 2,
      'moveRightMultiplier': 3,
    },
    {
      'left': 2 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '19',
      'color': Colors.grey,
      'moveLeftMultiplier': 3,
      'moveRightMultiplier': 2,
    },
    {
      'left': 1 * beadWidth,
      'position': POSITION_RIGHT,
      'text': '20',
      'color': Colors.grey,
      'moveLeftMultiplier': 4,
      'moveRightMultiplier': 1,
    },
  ];

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 60;

    List<Widget> beads = [];

    void moveBead(int i) {
      if (beadsConfig[i]['position'] == POSITION_RIGHT) {
        // if the previous bead is on the way, move it as well
        if (i > 0 && beadsConfig[i - 1]['position'] == POSITION_RIGHT) {
          moveBead(i - 1);
        }

        // move the bead left
        beadsConfig[i]['left'] =
            screenWidth - beadsConfig[i]['moveLeftMultiplier'] * beadWidth;

        // save new position
        beadsConfig[i]['position'] = POSITION_LEFT;

        return;
      }

      // if the next bead is on the way, move it as well
      if (i < 19 && beadsConfig[i + 1]['position'] == POSITION_LEFT) {
        moveBead(i + 1);
      }

      // move right
      beadsConfig[i]['left'] =
          beadsConfig[i]['moveRightMultiplier'] * beadWidth;

      // save new position
      beadsConfig[i]['position'] = POSITION_RIGHT;
    }

    for (int i = 0; i < 20; i++) {
      beads.add(AnimatedPositioned(
        duration: Duration(milliseconds: 300),
        left: screenWidth - beadsConfig[i]['left'],
        child: CircleButton(
          text: beadsConfig[i]['text'],
          color: beadsConfig[i]['color'],
          onTap: () {
            setState(() {
              moveBead(i);
            });
          },
        ),
      ));
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: 74,
              child: Padding(
                padding: EdgeInsets.only(
                  left: 30.0,
                  right: 30.0,
                  top: 30.0,
                  bottom: 4.0,
                ),
                child: Stack(
                  children: [
                    AbacusDivider(),
                    beads[0],
                    beads[1],
                    beads[2],
                    beads[3],
                    beads[4],
                  ],
                ),
              ),
            ),
            Container(
              height: 48,
              child: Padding(
                padding: EdgeInsets.only(
                  left: 30.0,
                  right: 30.0,
                  top: 4.0,
                  bottom: 4.0,
                ),
                child: Stack(
                  children: [
                    AbacusDivider(),
                    beads[5],
                    beads[6],
                    beads[7],
                    beads[8],
                    beads[9],
                  ],
                ),
              ),
            ),
            Container(
              height: 48,
              child: Padding(
                padding: EdgeInsets.only(
                  left: 30.0,
                  right: 30.0,
                  top: 4.0,
                  bottom: 4.0,
                ),
                child: Stack(
                  children: [
                    AbacusDivider(),
                    beads[10],
                    beads[11],
                    beads[12],
                    beads[13],
                    beads[14],
                  ],
                ),
              ),
            ),
            Container(
              height: 74,
              child: Padding(
                padding: EdgeInsets.only(
                  left: 30.0,
                  right: 30.0,
                  top: 4.0,
                  bottom: 30.0,
                ),
                child: Stack(
                  children: [
                    AbacusDivider(),
                    beads[15],
                    beads[16],
                    beads[17],
                    beads[18],
                    beads[19],
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AbacusDivider extends StatelessWidget {
  const AbacusDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ShaderMask(
        shaderCallback: (Rect bounds) {
          return LinearGradient(
            colors: [Colors.black12, Colors.black],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ).createShader(bounds);
        },
        child: Divider(
          thickness: 3,
          color: Colors.black26,
        ),
      ),
    );
  }
}

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final String text;
  final Color color;

  const CircleButton(
      {required this.onTap, required this.text, required this.color});

  @override
  Widget build(BuildContext context) {
    double size = 40.0;

    return GestureDetector(
      onTap: onTap,
      child: ShaderMask(
        shaderCallback: (Rect bounds) {
          return RadialGradient(
            colors: [Colors.white, color],
          ).createShader(bounds);
        },
        child: Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            color: color,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0.3,
                blurRadius: 1,
              )
            ],
          ),
          child: Center(
            child: Text(
              '',
              style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
      ),
    );
  }
}
