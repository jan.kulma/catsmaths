import 'package:flutter/material.dart';

class AnimatedButton extends StatefulWidget {
  final Widget child;
  final Color animationColor;
  final Function onTapCallback;

  const AnimatedButton({
    required this.child,
    required this.animationColor,
    required this.onTapCallback,
  });

  @override
  _AnimatedButtonState createState() => _AnimatedButtonState();
}

class _AnimatedButtonState extends State<AnimatedButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _colorTween;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 150),
    );
    _colorTween = ColorTween(
      begin: Colors.black,
      end: widget.animationColor,
    ).animate(_animationController);

    _colorTween.addListener(() {
      setState(() {});
    });
    _colorTween.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _animationController.reverse();
      }
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void _onTap() {
    _animationController.forward();
    widget.onTapCallback();
  }

  @override
  Widget build(BuildContext context) {
    double _scale = (_animationController.value / 1.5) + 1;

    return GestureDetector(
      onTap: _onTap,
      child: Transform.scale(
        scale: _scale,
        child: ColorFiltered(
          colorFilter: ColorFilter.mode(_colorTween.value, BlendMode.srcIn),
          child: widget.child,
        ),
      ),
    );
  }
}
