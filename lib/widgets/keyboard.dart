import 'dart:async';

import 'package:catsmaths/constants.dart';
import 'package:catsmaths/widgets/animated_button.dart';
import 'package:catsmaths/widgets/custom_icons.dart';
import 'package:flutter/material.dart';

const String KEY_1 = '1';
const String KEY_2 = '2';
const String KEY_3 = '3';
const String KEY_4 = '4';
const String KEY_5 = '5';
const String KEY_6 = '6';
const String KEY_7 = '7';
const String KEY_8 = '8';
const String KEY_9 = '9';
const String KEY_0 = '0';
const String KEY_CLEAR = 'clear';
const String KEY_CHECK = 'check';

const List<String> ACTION_KEYS = [KEY_CLEAR, KEY_CHECK];

class KeyboardController {
  StreamController inputStreamController = new StreamController.broadcast();

  void setInput(entered) {
    inputStreamController.add(entered);
  }

  Stream get keyboardInput => inputStreamController.stream;
}

class Keyboard extends StatelessWidget {
  final KeyboardController controller;

  const Keyboard({required this.controller});

  @override
  Widget build(BuildContext context) {
    Map<String, Widget> keys = {
      KEY_1: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_1,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[0],
          onTapCallback: () {
            controller.setInput(KEY_1);
          },
        ),
      ),
      KEY_2: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_2,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[1],
          onTapCallback: () {
            controller.setInput(KEY_2);
          },
        ),
      ),
      KEY_3: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_3,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[2],
          onTapCallback: () {
            controller.setInput(KEY_3);
          },
        ),
      ),
      KEY_4: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_4,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[3],
          onTapCallback: () {
            controller.setInput(KEY_4);
          },
        ),
      ),
      KEY_5: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_5,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[4],
          onTapCallback: () {
            controller.setInput(KEY_5);
          },
        ),
      ),
      KEY_6: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_6,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[5],
          onTapCallback: () {
            controller.setInput(KEY_6);
          },
        ),
      ),
      KEY_7: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_7,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[6],
          onTapCallback: () {
            controller.setInput(KEY_7);
          },
        ),
      ),
      KEY_8: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_8,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[7],
          onTapCallback: () {
            controller.setInput(KEY_8);
          },
        ),
      ),
      KEY_9: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_9,
            style: kMathsKeyboardStyle,
          ),
          animationColor: kRainbowColors[8],
          onTapCallback: () {
            controller.setInput(KEY_9);
          },
        ),
      ),
      KEY_0: KeyboardButton(
        child: AnimatedButton(
          child: Text(
            KEY_0,
            style: kMathsKeyboardStyle,
          ),
          animationColor: Colors.grey,
          onTapCallback: () {
            controller.setInput(KEY_0);
          },
        ),
      ),
      KEY_CLEAR: KeyboardButton(
        child: AnimatedButton(
          child: Icon(
            CustomIcons.times,
            size: kMathsKeyboardButtonIconSize,
          ),
          animationColor: Colors.red,
          onTapCallback: () {
            controller.setInput(KEY_CLEAR);
          },
        ),
      ),
      KEY_CHECK: KeyboardButton(
        child: AnimatedButton(
          child: Icon(
            CustomIcons.check,
            size: kMathsKeyboardButtonIconSize,
          ),
          animationColor: Colors.green,
          onTapCallback: () {
            controller.setInput(KEY_CHECK);
          },
        ),
      ),
    };

    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            keys[KEY_1]!,
            keys[KEY_2]!,
            keys[KEY_3]!,
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            keys[KEY_4]!,
            keys[KEY_5]!,
            keys[KEY_6]!,
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            keys[KEY_7]!,
            keys[KEY_8]!,
            keys[KEY_9]!,
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            keys[KEY_CLEAR]!,
            keys[KEY_0]!,
            keys[KEY_CHECK]!,
          ],
        ),
      ],
    );
  }
}

class KeyboardButton extends StatelessWidget {
  final Widget child;

  const KeyboardButton({required this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 65.0,
      height: 65.0,
      child: Center(
        child: child,
      ),
    );
  }
}
