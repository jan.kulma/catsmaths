import 'package:flutter/material.dart';

var rainbow = BoxDecoration(
  gradient: RainbowGradient(),
);

class RainbowGradient extends BaseRainbowGradient {
  RainbowGradient()
      : super(
          colors: kRainbowColors,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        );
}

class BaseRainbowGradient extends LinearGradient {
  BaseRainbowGradient({
    required List<Color> colors,
    AlignmentGeometry begin = Alignment.topLeft,
    AlignmentGeometry end = Alignment.topRight,
  }) : super(
          begin: begin,
          end: end,
          colors: _buildColors(colors),
          stops: _buildStops(colors),
        );

  static List<Color> _buildColors(List<Color> colors) {
    return colors.fold<List<Color>>(<Color>[],
        (List<Color> list, Color color) => list..addAll(<Color>[color, color]));
  }

  static List<double> _buildStops(List<Color> colors) {
    final List<double> stops = <double>[0.0];

    for (int i = 1, len = colors.length; i < len; i++) {
      stops.add(i / colors.length);
      stops.add(i / colors.length);
    }

    return stops..add(1.0);
  }
}

const kLogoTextStyle = TextStyle(
  fontSize: 60.0,
  fontWeight: FontWeight.w700,
  fontFamily: 'WashYourHand',
);

const List<Color> kRainbowColors = [
  Color(0xFFFF0064),
  Color(0xFFFF7600),
  Color(0xFFFFD500),
  Color(0xFF8CFE00),
  Color(0xFF00E86C),
  Color(0xFF00F4F2),
  Color(0xFF00CCFF),
  Color(0xFF70A2FF),
  Color(0xFFA96CFF),
];

const double kMathsKeyboardButtonSize = 50.0;
const double kMathsKeyboardButtonIconSize = 40.0;
const kMathsKeyboardStyle = TextStyle(
  fontSize: kMathsKeyboardButtonSize,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);

List<TextSpan> getRainbowText(String text) {
  List<String> letters = text.split('');
  List<TextSpan> spans = [];

  int colorIndex = 0;
  for (var i = 0; i < letters.length; i++) {
    if (colorIndex >= kRainbowColors.length) {
      colorIndex = 0;
    }

    spans.add(
      TextSpan(
        text: letters[i],
        style: TextStyle(color: kRainbowColors[colorIndex]),
      ),
    );
    colorIndex++;
  }
  return spans;
}

RichText getRainbowText2(String text, {TextStyle style = kLogoTextStyle}) {
  List<String> letters = text.split('');
  List<TextSpan> spans = [];

  int colorIndex = 0;
  for (var i = 0; i < letters.length; i++) {
    if (colorIndex >= kRainbowColors.length) {
      colorIndex = 0;
    }

    spans.add(
      TextSpan(
        text: letters[i],
        style: TextStyle(color: kRainbowColors[colorIndex]),
      ),
    );
    colorIndex++;
  }
  return RichText(
    text: TextSpan(
      style: style,
      children: spans,
    ),
  );
}

const kSettingsLevelTextStyle =
    TextStyle(fontSize: 30.0, fontWeight: FontWeight.w500);
