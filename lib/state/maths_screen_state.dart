import 'package:catsmaths/widgets/settings.dart';
import 'package:flutter/cupertino.dart';

class MathsScreenState extends ChangeNotifier {
  DifficultyLevel difficultyLevel = DifficultyLevel.cat;

  void setDifficultyLevel(DifficultyLevel level) {
    difficultyLevel = level;
    notifyListeners();
  }
}
