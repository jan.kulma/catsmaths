import 'dart:convert';
import 'dart:math';

import 'package:catsmaths/controllers/safe_gif_ids.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class GifController {
  Image? gif;

  void preloadGif(BuildContext context) async {
    if (gif != null) {
      return;
    }

    Random _random = Random();
    List<String> safeIds = getSafeIds();
    String id = safeIds[_random.nextInt(safeIds.length)];
    Response response = await get(Uri.parse(
        'https://api.giphy.com/v1/gifs/$id?api_key=sCHShsdz6dumBduWcktFr2OmXSIrBsS4'));
    if (response.statusCode != 200) {
      throw 'giphy api: invalid response';
    }

    var decodedData = jsonDecode(response.body);
    var gifUrl = decodedData['data']['images']['downsized_large']['url'];

    gif = Image.network(
      gifUrl,
      loadingBuilder: (BuildContext context, Widget child,
          ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes!
                : null,
          ),
        );
      },
    );

    await precacheImage(gif!.image, context);
  }
}
