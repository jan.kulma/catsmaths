import 'package:catsmaths/screens/maths.dart';
import 'package:catsmaths/screens/welcome.dart';
import 'package:catsmaths/state/maths_screen_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => MathsScreenState()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CatsMaths',
      initialRoute: WelcomeScreen.id,
      routes: {
        WelcomeScreen.id: (context) => WelcomeScreen(),
        MathsScreen.id: (context) => MathsScreen(),
      },
    );
  }
}
