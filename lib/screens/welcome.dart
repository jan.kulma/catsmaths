import 'package:catsmaths/constants.dart';
import 'package:catsmaths/screens/maths.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  static String id = 'welcome';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Image(
                  image: AssetImage('assets/lily.png'),
                  width: 150.0,
                ),
                SizedBox(
                  height: 20.0,
                ),
                getRainbowText2('CatsMaths'),
              ],
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, MathsScreen.id);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    width: 220,
                    height: 220,
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: Container(
                            width: 200,
                            height: 200,
                            decoration: BoxDecoration(
                              gradient: RainbowGradient(),
                              borderRadius: BorderRadius.circular(100),
                            ),
                          ),
                        ),
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          child: Image(
                            image: AssetImage('assets/cat.png'),
                            height: 150,
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          child: Image(
                            image: AssetImage('assets/lion.png'),
                            height: 150,
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Image(
                            image: AssetImage('assets/tiger.png'),
                            height: 150,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Play!',
                    style:
                        TextStyle(fontSize: 50.0, fontFamily: 'WashYourHand'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
