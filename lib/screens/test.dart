import 'dart:async';
import 'dart:math';

import 'package:catsmaths/constants.dart';
import 'package:catsmaths/screens/gif.dart';
import 'package:catsmaths/widgets/keyboard.dart';
import 'package:catsmaths/widgets/math_problem.dart';
import 'package:flutter/material.dart';

class Test extends StatefulWidget {
  static String id = 'test';

  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  KeyboardController keyboardController = KeyboardController();
  MathProblemController mathProblemController = MathProblemController();

  @override
  void initState() {
    super.initState();
    keyboardController.keyboardInput.listen((input) {
      mathProblemController.processInput(input);
      setState(() {});
    });
    mathProblemController.mathProblemStream.listen((event) {
      if (event == MathProblemController.EVENT_CORRECT_ANSWER) {
        Timer(
          Duration(milliseconds: 800),
          () => Navigator.pushNamed(context, GifScreen.id),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MathProblemView(
            controller: mathProblemController,
          ),
          SizedBox(
            height: 40.0,
          ),
          Keyboard(
            controller: keyboardController,
          ),
        ],
      ),
    );
  }
}
