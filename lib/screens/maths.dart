import 'dart:async';

import 'package:catsmaths/constants.dart';
import 'package:catsmaths/controllers/gif_controller.dart';
import 'package:catsmaths/screens/gif.dart';
import 'package:catsmaths/state/maths_screen_state.dart';
import 'package:catsmaths/widgets/abacus.dart';
import 'package:catsmaths/widgets/animated_button.dart';
import 'package:catsmaths/widgets/custom_icons.dart';
import 'package:catsmaths/widgets/keyboard.dart';
import 'package:catsmaths/widgets/math_problem.dart';
import 'package:catsmaths/widgets/settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MathsScreen extends StatefulWidget {
  static String id = 'maths';

  @override
  _MathsScreenState createState() => _MathsScreenState();
}

class _MathsScreenState extends State<MathsScreen> {
  KeyboardController keyboardController = KeyboardController();
  MathProblemController mathProblemController = MathProblemController();
  GifController gifController = GifController();
  DifficultyLevel difficultyLevel = DifficultyLevel.cat;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    keyboardController.keyboardInput.listen((input) {
      mathProblemController.processInput(input);
      setState(() {});
    });

    mathProblemController.mathProblemStream.listen((event) {
      if (event == MathProblemController.EVENT_CORRECT_ANSWER) {
        Timer(
          Duration(milliseconds: 800),
          () => Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => GifScreen(gif: gifController.gif!),
            ),
          ),
        );
        return;
      }
      if (event == MathProblemController.EVENT_DIFFICULTY_CHANGED) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    gifController.preloadGif(context);

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: MathProblemView(
              controller: mathProblemController,
            ),
          ),
          Keyboard(
            controller: keyboardController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SettingsButton(),
              SizedBox(
                width: 50,
              ),
              KeyboardButton(
                child: AnimatedButton(
                  child: Icon(
                    CustomIcons.abacus,
                    size: kMathsKeyboardButtonIconSize,
                  ),
                  animationColor: kRainbowColors[3],
                  onTapCallback: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (context) => Abacus(),
                    );
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class SettingsButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DifficultyLevel level = context.watch<MathsScreenState>().difficultyLevel;

    String imgPath;

    switch (level) {
      case DifficultyLevel.cat:
        imgPath = 'assets/cat.png';
        break;
      case DifficultyLevel.tiger:
        imgPath = 'assets/tiger.png';
        break;
      case DifficultyLevel.lion:
        imgPath = 'assets/lion.png';
        break;
    }

    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          builder: (context) => Settings(),
        );
      },
      child: Stack(
        alignment: Alignment.bottomRight,
        children: <Widget>[
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              gradient: RainbowGradient(),
              borderRadius: BorderRadius.circular(50),
            ),
            child: Image(
              image: AssetImage(imgPath),
              height: 100,
            ),
          ),
          Icon(
            Icons.settings,
            size: 30,
          )
        ],
      ),
    );
  }
}
