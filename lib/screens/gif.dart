import 'package:catsmaths/constants.dart';
import 'package:catsmaths/screens/maths.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GifScreen extends StatelessWidget {
  static String id = 'gif';
  final Image gif;

  const GifScreen({required this.gif});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: rainbow,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30.0),
                      child: gif,
                    ),
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 20.0,
                      ),
                      backgroundColor: Colors.white,
                    ),
                    child: RichText(
                      text: TextSpan(
                        style: kLogoTextStyle.copyWith(
                          fontSize: 45.0,
                        ),
                        children: getRainbowText('Next!'),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, MathsScreen.id);
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0, top: 30.0),
                    child: Image(
                      image: AssetImage('assets/giphy-attribution.png'),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
