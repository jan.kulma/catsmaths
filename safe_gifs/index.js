var searchTerm = 'funny-cats';
var getRandomGifUrl = 'http://api.giphy.com/v1/gifs/random?api_key=sCHShsdz6dumBduWcktFr2OmXSIrBsS4&tag=' + searchTerm;
var searchGifsUrl = 'http://api.giphy.com/v1/gifs/search?api_key=sCHShsdz6dumBduWcktFr2OmXSIrBsS4&rating=g&offset=1&q=' + searchTerm
var gifId;

// url = 'https://api.gfycat.com/v1/gfycats/search?search_text=funny+cat';
// https://api.gfycat.com/v1/gfycats/<id>

function nextGif() {
  console.log('loading');
  $.ajax(getRandomGifUrl)
  .done(function(response) {
    var gifUrl = response.data.images.downsized_large.url;
    gifId = response.data.id
    var img = $('<img>'); //Equivalent: $(document.createElement('img'))
    img.attr('src', gifUrl);
    $('#gif').empty();
    img.appendTo('#gif');
    console.log('done');
  })
  .fail(function() {
    console.log('lol error');
  })
}

function addId(id) {
  $('#ids').append("'" + id + "'" + ', ');
}

function search() {
console.log('searching');
  $.ajax(searchGifsUrl)
    .done(function(response) {
        response.data.forEach(item => {
            const gifUrl = item.images.downsized_large.url
            const gifId = item.id
            const imgEl = $('<img>')
            imgEl.attr('src', gifUrl)

            imgEl.click(() => {
                addId(gifId)
            })

            imgEl.appendTo('#gif')
        })
  })
  .fail(function() {
    console.log('lol error');
  })
}

$(document).ready(function() {
  $(document).keyup(function(event) {
    if (event.keyCode === 39) { // right
      console.log('next');
      nextGif();
    }
    if (event.keyCode === 40) { // down
      console.log('save');
      addId(gifId);
    }
    if (event.keyCode === 13) { // enter
      console.log('search');
      search();
    }
  });
});
